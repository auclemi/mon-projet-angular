import {Component, OnInit, Input, Output} from '@angular/core';
import {AppareilService} from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {
  @Input() appareilStatus: string;
  @Input() index: number;
  @Input() id: number;

  @Input()
  set appareilName(value: string) {
    this._appareilName = value;
    //do something
  }

  @Input()
  get appareilName(value: string) {
    return this._appareilName;
  }

  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date().toISOString();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });

  constructor(private appareilService: AppareilService) {
  }

  ngOnInit() {
  }


  getStatus() {
    return this.appareilStatus;
  }

  getColor() {
    if (this.appareilStatus === 'allumé') {
      return 'green';
    } else if (this.appareilStatus === 'éteint') {
      return 'red';
    }
  }


  onSwitch() {
    if (this.appareilStatus === 'allumé') {
      this.appareilService.switchOffOne(this.index);
    } else if (this.appareilStatus === 'éteint') {
      this.appareilService.switchOnOne(this.index);
    }
  }

  onNameBlur() {
    this.appareilService.setName(this.appareilName, this.index);
  }
}
